import Vue from 'vue'
import Router from 'vue-router'

import AboutPage from '../components/about';

import HomePage from "../components/HelloWorld";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "HomePage",
      component: HomePage
    },
    {
      path: '/about-us',
      name: 'Aboutus',
      component:AboutPage
   }
  ]
});
